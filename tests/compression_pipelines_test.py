"""
Tests for non-TSC compression pipelines (and TSC against FSDD data).
"""

import os

import numpy as np
import pytest

from tsc import tsc_pipeline
from tsc.utils.compression_pipelines import (
    dft_pipeline,
    opus_pipeline,
    paa_pipeline,
    random_pipeline,
)
from tsc.utils.data import fsdd


@pytest.fixture(scope="module")
def test_input() -> np.ndarray:
    """
    Test input for compression pipelines.
    """
    return np.array([1, -1, 5, -3, -2])


@pytest.fixture(scope="module")
def expected_random_result() -> np.ndarray:
    """
    Return expected result when running random pipeline.
    """
    return np.array([1, -1, 5, 1.5, -2])


@pytest.fixture(scope="module")
def expected_paa_result() -> np.ndarray:
    """
    Return expected result when running PAA pipeline.
    """
    return np.array([-0.25, 0.25, 0.75, -0.75, -4.25])


@pytest.fixture(scope="module")
def fsdd_signal() -> np.ndarray:
    """
    Return example FSDD signal.
    """
    return fsdd(1)[0][0]


@pytest.fixture(scope="module")
def sampling_rate() -> int:
    """
    return FSDD sampling rate.
    """
    return 8000


class TestPipelineFunctionsSpecificValues:
    """
    Tests for the more interpretable pipeline functions (e.g. not opus or dft) on getting some specific values out.
    """

    @pytest.mark.parametrize("n_keep", [4, 0.8])
    def test_random_pipeline(
        self,
        test_input: np.ndarray,
        n_keep: float,
        expected_random_result: np.ndarray,
    ) -> None:
        """
        Make sure ``tsc.utils.compression_pipelines.random_pipeline()`` gives us the expected results for a toy example.

        :param test_input: test input to run through compression.
        :param n_keep: number of points to keep.
        :param expected_random_result: expected compressed result from random pipeline run.
        """
        assert np.all(
            random_pipeline(signal=test_input, n_keep=n_keep, random_seed=1)
            == expected_random_result
        )

    def test_paa_pipeline(
        self,
        test_input: np.ndarray,
        expected_paa_result: np.ndarray,
    ) -> None:
        """
        Make sure ``tsc.utils.compression_pipelines.paa_pipeline()`` gives us the expected results for a toy example.

        :param test_input: test input to run through compression.
        :param expected_paa_result: expected compressed result from PAA pipeline run.
        """
        assert np.all(
            paa_pipeline(signal=test_input, window_size=2) == expected_paa_result
        )

    @pytest.mark.parametrize("func", [tsc_pipeline, random_pipeline])
    def test_pipelines_no_compression(
        self,
        func: callable,
        test_input: np.ndarray,
    ) -> None:
        """
        Make sure pipelines that don't require compression (e.g. with ``n_keep``) return the original signal.

        :param func: which compression pipeline to run.
        :param test_input: test input to run through compression.
        """
        assert np.array_equal(func(signal=test_input, n_keep=1e6), test_input)

    def test_pipelines_no_compression_paa(
        self,
        test_input: np.ndarray,
    ) -> None:
        """
        Confirm ``tsc.utils.compression_pipelines.paa_pipeline()`` call with no compression returns the original signal.

        (e.g. ``window_size=1``).

        :param test_input: test input to run through compression.
        """
        assert np.array_equal(
            paa_pipeline(signal=test_input, window_size=1), test_input
        )


class TestPipelineFSDDExample:
    """
    Tests for all of the pipeline functions on a single FSDD example.
    """

    def test_tsc_pipeline(
        self,
        fsdd_signal: np.ndarray,
    ) -> None:
        """
        Make sure ``tsc.tsc_pipeline()`` behaves as expected for a more complicated example.

        :param fsdd_signal: FSDD signal fixture.
        """
        # run at higher compressions since there's a minimum compression of "number of critical points" for TSC
        out_low_compression = tsc_pipeline(signal=fsdd_signal, n_keep=0.5)
        out_high_compression = tsc_pipeline(signal=fsdd_signal, n_keep=0.2)

        # reconstructs to correct size
        assert out_low_compression.size == out_high_compression.size == fsdd_signal.size

        # more compressed is more "wrong"
        assert (
            np.abs(out_high_compression - fsdd_signal).sum()
            > np.abs(out_low_compression - fsdd_signal).sum()
        )

    def test_dft_pipeline(self, fsdd_signal: np.ndarray) -> None:
        """
        Make sure ``tsc.utils.compression_pipelines.dft_pipeline()`` behaves as expected for a more complicated example.

        :param fsdd_signal: FSDD signal fixture.
        """
        out_low_compression = dft_pipeline(signal=fsdd_signal, percent_compressed=20)
        out_high_compression = dft_pipeline(signal=fsdd_signal, percent_compressed=50)

        # reconstructs to correct size
        assert out_low_compression.size == out_high_compression.size == fsdd_signal.size

        # more compressed is more "wrong"
        assert (
            np.abs(out_high_compression - fsdd_signal).sum()
            > np.abs(out_low_compression - fsdd_signal).sum()
        )

    def test_opus_pipeline(
        self,
        fsdd_signal: np.ndarray,
        sampling_rate: int,
    ) -> None:
        """
        Make sure ``tsc.utils.compression_pipelines.opus_pipeline()`` behaves as expected.

        This test runs on a more complicated example.

        :param fsdd_signal: FSDD signal fixture.
        :param sampling_rate: sampling rate fixture.
        """
        log_file = "data/pytest/test_opus_pipeline_log.txt"

        less_compressed_wav = "data/pytest/test_opus_pipeline_less.wav"
        more_compressed_wav = "data/pytest/test_opus_pipeline_more.wav"

        # auto-generated `.opus` files will name match `.wav` files
        less_compressed_opus = os.path.join(
            os.path.dirname(less_compressed_wav),
            f"{os.path.basename(less_compressed_wav).split('.')[0]}.opus",
        )
        more_compressed_opus = os.path.join(
            os.path.dirname(more_compressed_wav),
            f"{os.path.basename(more_compressed_wav).split('.')[0]}.opus",
        )

        out_low_compression = opus_pipeline(
            signal=fsdd_signal,
            wav_path=less_compressed_wav,
            bitrate=90,
            log_file=log_file,
            sampling_rate=sampling_rate,
        )
        out_high_compression = opus_pipeline(
            signal=fsdd_signal,
            wav_path=more_compressed_wav,
            bitrate=8,
            log_file=log_file,
            sampling_rate=sampling_rate,
        )

        # reconstructs to correct size
        assert out_low_compression.size == out_high_compression.size == fsdd_signal.size

        # more compressed is more "wrong"
        assert (
            np.abs(out_high_compression - fsdd_signal).sum()
            > np.abs(out_low_compression - fsdd_signal).sum()
        )

        # kill the files on disk
        os.remove(less_compressed_wav)
        os.remove(less_compressed_opus)

        os.remove(more_compressed_wav)
        os.remove(more_compressed_opus)

        os.remove(log_file)

    def test_paa_pipeline(
        self,
        fsdd_signal: np.ndarray,
    ) -> None:
        """
        Make sure ``tsc.utils.compression_pipelines.paa_pipeline()`` behaves as expected for a more complicated example.

        :param fsdd_signal: FSDD signal fixture.
        """
        out_low_compression = paa_pipeline(signal=fsdd_signal, window_size=2)
        out_high_compression = paa_pipeline(signal=fsdd_signal, window_size=10)

        # reconstructs to correct size
        assert out_low_compression.size == out_high_compression.size == fsdd_signal.size

        # more compressed is more "wrong"
        assert (
            np.abs(out_high_compression - fsdd_signal).sum()
            > np.abs(out_low_compression - fsdd_signal).sum()
        )

    def test_random_pipeline(
        self,
        fsdd_signal: np.ndarray,
    ) -> None:
        """
        Make sure ``tsc.utils.compression_pipelines.random_pipeline()`` behaves as expected.

        This test runs on a more complicated example.

        :param fsdd_signal: FSDD signal fixture.
        """
        out_low_compression = random_pipeline(
            signal=fsdd_signal, n_keep=0.8, random_seed=1
        )
        out_high_compression = random_pipeline(
            signal=fsdd_signal, n_keep=0.5, random_seed=1
        )

        # reconstructs to correct size
        assert out_low_compression.size == out_high_compression.size == fsdd_signal.size

        # more compressed is more "wrong"
        assert (
            np.abs(out_high_compression - fsdd_signal).sum()
            > np.abs(out_low_compression - fsdd_signal).sum()
        )
