"""
Tests for ``tsc.utils.__init__.py``.
"""

import os

import numpy as np
from scipy.io.wavfile import write

from tsc.utils.helper_functions import min_max_normalize, pydub_to_numpy


def test_min_max_normalize() -> None:
    """
    Make sure ``tsc.utils.min_max_normalize()`` behaves as expected.
    """
    arr = np.arange(-5, 5)

    new_arr = min_max_normalize(arr)

    # types should be float32
    #  (used to make all types consistent among compressions when checking compression size on disk)
    assert isinstance(new_arr[0], np.float32)

    # should be normalized into [0, 1]
    assert arr.min() < 0
    assert new_arr.min() == 0
    assert arr.max() > 1
    assert new_arr.max() == 1


def test_pydub_to_numpy() -> None:
    """
    Make sure ``tsc.utils.pydub_to_numpy()`` behaves as expected.
    """
    rng = np.random.default_rng(0)
    size = 1000
    arr = min_max_normalize(rng.random(size=size))

    # build out wav file to read in as audiosegment
    f = os.path.join("data/pytest", "test_pydub_to_numpy.wav")
    write(f, rate=size, data=arr)
    from pydub import AudioSegment

    audiosegment = AudioSegment.from_wav(f)

    # kill the file on disk before checking anything once it's in memory
    os.remove(f)

    # normalize to keep things consistent
    out_arr = min_max_normalize(pydub_to_numpy(audiosegment=audiosegment))
    assert out_arr.shape == arr.shape
    assert np.allclose(arr, out_arr, rtol=0.05)
