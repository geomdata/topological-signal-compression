# data/pytest/failed_notebooks

Any notebooks that fail to run end-to-end in the test-suite will be saved as html files in this directory (and will be gitignored). 