#!/bin/bash

# copy over example notebooks
cp notebooks/*.ipynb docs/source/
# build up from scratch, put in dummy dir to move out html in CI
if [ $1 = "-W" ] 
then
    echo "Building docs NOT allowing any warnings (e.g. with -W)" 
    python -m sphinx docs/source public -j4 -a -W
else
    echo "Building docs allowing warnings (e.g. without -W)"
    python -m sphinx docs/source public -j4 -a
fi
