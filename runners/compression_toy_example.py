"""
An example showing how persistent signal compression preserves structure using a random walk.
"""

import datetime
import os
from copy import copy
from glob import glob

import dateutil.tz
import matplotlib.pyplot as plt
import numpy as np

from tsc import compress_tsc, signal_persistence, tsc_pipeline
from tsc.utils.viz import plot_persistence

# setup

# length of signal
SIGNAL_LENGTH = 1000

# plot removing every `SKIP` persistence values
SKIP = 40

# two different positive / negative values
CHOICES = [-1, -0.5, 0.5, 1]

# subset of indices to look at to observe lost information when compressing with persistence
SUB_MIN = 150
SUB_MAX = 200

# make timestamped folder
NOW = datetime.datetime.now(dateutil.tz.tzlocal())
DATETIME_STAMP = NOW.strftime("%Y%m%d_%H%M%S_%f")

# out_dir
OUT_PATH = os.path.join("./data", "signal_compression_{}".format(DATETIME_STAMP))
os.mkdir(OUT_PATH)

# where the frames of the gif will be stored
IMAGE_PATH = os.path.join(OUT_PATH, "images")
os.mkdir(IMAGE_PATH)

# building and compressing signal

rng = np.random.default_rng(27703)
# random walk plus a little bit of noise to separate values on pers diagram
data = np.cumsum(rng.choice(CHOICES, SIGNAL_LENGTH)) + rng.normal(0, 0.1, SIGNAL_LENGTH)
times = np.arange(data.size)

# plot original figure
fig, ax = plt.subplots(figsize=(10, 4))
ax.plot(times, data, c="black", lw=0.8)
ax.set_title(f"Original Signal ({SIGNAL_LENGTH} Data Points)")
plt.savefig(os.path.join(OUT_PATH, "original_signal.png"), bbox_inches="tight")
plt.close()

pers = signal_persistence(np.c_[times, data])

# plot compressed figure with only persistence diagram
compressed_signal = tsc_pipeline(signal=data, pers_diag=pers, n_keep="all")

fig, ax = plt.subplots(figsize=(10, 4))
ax.plot(times, compressed_signal, c="black", lw=0.8)
ax.set_title(
    f"Signal Reconstructed from Persistence Diagram ({pers.shape[0]} Persistence Values)"
)
plt.savefig(os.path.join(OUT_PATH, "full_pers_signal.png"), bbox_inches="tight")
plt.close()

# plot the compressed with the original
fig, ax = plt.subplots(figsize=(10, 4))
ax.plot(times, data, label="Original Signal")
ax.plot(times, compressed_signal, "--", label="Reconstructed Signal")
ax.set_title('Reconstructed Signal "Holds True" to Original Signal')
ax.legend(loc="upper left", bbox_to_anchor=(1, 1))
plt.savefig(
    os.path.join(OUT_PATH, "original_with_compressed_signal.png"), bbox_inches="tight"
)
plt.close()

# plot subset to demonstrate only capturing critical points (missing changes that aren't critical points)
fig, ax = plt.subplots(figsize=(10, 4))
ax.plot(times[SUB_MIN:SUB_MAX], data[SUB_MIN:SUB_MAX], lw=2, label="Original Signal")
ax.plot(
    times[SUB_MIN:SUB_MAX],
    compressed_signal[SUB_MIN:SUB_MAX],
    "--",
    label="Reconstructed Signal",
    lw=2,
    alpha=0.8,
)
ax.set_title(
    "Some Information Lost When Reconstructing Signal with Persistence Diagram"
)
ax.legend(loc="upper left", bbox_to_anchor=(1, 1))
plt.savefig(
    os.path.join(OUT_PATH, "original_with_compressed_signal_subset.png"),
    bbox_inches="tight",
)
plt.close()

# reference original persistence diagram
original_pers = copy(pers)

# loop over removing  every `SKIP` pers values and reconstructing signal (and check with just 2 values e.g. endpoints)
num_pers_values_removed = np.arange(0, pers.shape[0], SKIP)
for i, val in enumerate(num_pers_values_removed):
    # threshold the persistence diagram

    # build compressed signal via subset of original persistence diagram
    compressed_signal = compress_tsc(
        signal=np.c_[times, data],
        pers_diag=pers,
        num_indices_to_keep=pers.shape[0] - val,
    )

    fig = plt.figure(figsize=(10, 7))
    gs = fig.add_gridspec(2, 3)
    ax0 = fig.add_subplot(gs[0, :])
    ax1 = fig.add_subplot(gs[1, 1])

    ax0.plot(times, data, c="black", alpha=0.8, lw=1, label="Original Signal")
    ax0.plot(
        compressed_signal[:, 0],
        compressed_signal[:, 1],
        c="red",
        lw=0.8,
        label="Reconstructed Signal",
    )
    ax0.legend(loc="upper right")

    # don't need number of points for holoviews title
    title = "Signal Reconstructed from Partial Persistence Diagram"

    # grammar handling for number of points for matplotlib title
    mpl_title = title + f"\n({pers.shape[0]-val} Persistence Value"
    if pers.shape[0] - val == 1:
        mpl_title += ")"
    else:
        mpl_title += "s)"

    ax0.set_title(mpl_title)

    # corresponding pers diagram
    # work with a sorted persistence diagram to remove the lowest persistence points
    pers_diag = pers.copy().sort_values("pers", ascending=False)

    # know the index bounds so we can guarantee we maintain the domain
    start_index = times.min()
    stop_index = times.max()

    # take our desired subset of the persistence diagram for reconstruction
    pers_diag = pers_diag.iloc[: pers.shape[0] - val, :]

    # make sure we also have the edges of the signal
    indices = list(
        pers_diag.loc[:, ["birth_index", "death_index"]].to_numpy().flatten()
    )
    start_edge_contained = start_index in indices
    stop_edge_contained = stop_index in indices

    # but still the total desired number of indices
    num_vals_to_drop = int(np.logical_not(start_edge_contained)) + int(
        np.logical_not(stop_edge_contained)
    )
    # remove the values from the original persistence diagram
    pers_diag_remaining = pers.sort_values("pers", ascending=False).iloc[
        : pers.shape[0] - val - num_vals_to_drop, :
    ]
    pers_diag_removed = pers.sort_values("pers", ascending=False).iloc[
        (pers.shape[0] - val - num_vals_to_drop) :, :
    ]

    ax1.axis("equal")
    plot_persistence(
        pers_diag_remaining,
        title="Signal Persistence",
        fig=fig,
        ax=ax1,
        color="black",
        alpha=0.5,
        label="Remaining\nValues",
    )
    ax1.scatter(
        pers_diag_removed.loc[:, "birth"].values,
        pers_diag_removed.loc[:, "death"].values,
        c="red",
        alpha=0.3,
        label="Removed\nValues",
    )

    ax1.legend(loc="lower right")

    plt.savefig(
        os.path.join(IMAGE_PATH, "signal_{:03}.png".format(i)), bbox_inches="tight"
    )
    plt.close()

# make gif
print("Making GIF")
os.system(
    "convert -delay 200> -loop 0 {} -delay 400 {} {}.gif".format(
        os.path.join(IMAGE_PATH, "signal_*"),
        sorted(glob(os.path.join(IMAGE_PATH, "signal_*")))[-1],
        os.path.join(OUT_PATH, "compressed_signal"),
    )
)
