Topological Signal Compression
------------------------------

This package demonstrates a persistent homology-based compression of 1-dimensional signals,
which we call *Topological Signal Compression*.

.. image:: _static/logo.gif
   :width: 400

.. toctree::
   :caption: Compression and Reconstruction
   :maxdepth: 2

   intro_to_tsc
   fsdd
   music


.. toctree::
   :caption: TSC Documentation
   :maxdepth: 2

   autodoc

.. toctree::
   :caption: Release Notes
   :maxdepth: 2

   changelog
