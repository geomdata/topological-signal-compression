.. _changes:

TSC Changelog
=============

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog <https://keepachangelog.com/en/1.1.0/>`_,

As of May, 2023, this project adheres to `Semantic Versioning <https://semver.org/spec/v2.0.0.html>`_.

Version 0.11.2 (released Oct 22, 2024)
--------------------------------------

This release is to fix / improve tooling and add further Python support.

Changed
~~~~~~~

- Formal support only for Python 3.11 and 3.12.

- Code now run through ``ruff``.

Removed
~~~~~~~

- Dropping support for Python 3.8, following the timeline of Python's supported versions:
  https://devguide.python.org/versions/.

Version 0.11.1 (released May 27, 2023)
--------------------------------------

Enhancements
~~~~~~~~~~~~
- Added ``ImportError`` calls with messages to clarify when the user needs to install the ``[extras]`` dependencies.

- Improved type hints in docs.

- Made minor revisions to docstrings for clarity and flake8 compliance.

- ``tsc.utils.compression.compress_random()`` now sets a seed for random state by default.

- Updated internal calls setting random state in ``tsc.utils.compression.compress_random()`` and ``tsc.utils.data.fsdd()``
  from ``np.random.seed(<seed>)`` to ``rng = np.random.default_rng(<seed>)``.

- (Started a changelog.)

Dependency Changes
~~~~~~~~~~~~~~~~~~
- Removed the strict ``numpy`` version dependency. This was no longer required at the time of this release (it had been
  necessary in the past).
