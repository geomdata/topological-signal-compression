"""
Script to build gif of a toy signal, sweeping threshold up and observing connected components plus persistence diagram.
"""

import datetime
import os
from glob import glob
from typing import Tuple

import dateutil.tz
import matplotlib.pyplot as plt
import numpy as np
from homology.dim0 import all_roots
from matplotlib.axis import Axis
from matplotlib.figure import Figure
from timeseries import Signal

from tsc.utils.viz import plot_persistence

# hardcoded info

# whether to build single color below threshold or show connected components as different colors
SHOW_COMPONENTS = True

# what the single color should be if `SHOW_COMPONENTS = False`
SINGLE_COLOR = "C0"

# make timestamped folder
NOW = datetime.datetime.now(dateutil.tz.tzlocal())
DATETIME_STAMP = NOW.strftime("%Y%m%d_%H%M%S_%f")

# out_dir
OUT_PATH = os.path.join("./data", "signal_sweep_{}".format(DATETIME_STAMP))
os.mkdir(OUT_PATH)

# number of steps in gif
NUM_FRAMES = 20

# functions


def plot_signal_threshold(
    data: np.ndarray,
    tau: float,
    fig: Figure = None,
    ax: Axis = None,
    tau_color: str = "black",
    data_start_color: str = "gray",
    below_tau_color: str | list = "C0",
    above_tau_alpha: float = 0.2,
    figsize: tuple = (6, 4),
    title: str = "Signal",
) -> Tuple[Figure, Axis]:
    """
    Plot signal data at sweeping threshold ``tau``.

    :param data: ``(n, 2)`` array of ``[x, y]`` coordinates of signal data points.
    :param tau: value up to which we have swept up to from below the signal.
    :param fig: ``Figure`` onto which to plot. Default ``None`` makes own ``Figure``.
    :param ax: ``Axis`` onto which to plot. Default ``None`` makes own ``Axis``.
    :param tau_color: color for sweep line.
    :param data_start_color: color for data above tau.
    :param below_tau_color: color for data at or below tau.
        If single str input for color, will use same color for all points and lines below.
        If list input for color, will expect color for every data point.
    :param above_tau_alpha: alpha value for signal above tau.
    :param figsize: (horizontal, vertical) dimensions of figure.
    :param title: title of figure.
    :return: ``matplotlib`` figure, axis.
    :rtype: ``(Figure, Axis)``.
    """
    if fig is None and ax is None:
        fig, ax = plt.subplots(figsize=figsize)
    elif fig is not None and ax is not None:
        pass
    else:
        raise AssertionError("`fig` and `ax` must both be either `None` or not-`None`.")

    # coax 1d array input to list
    if isinstance(below_tau_color, np.ndarray):
        below_tau_color = list(below_tau_color)
    # color for every point
    if isinstance(below_tau_color, list):
        assert len(below_tau_color) == data.shape[0]
    # coax string to list of colors for each point\
    if isinstance(below_tau_color, str):
        below_tau_color = [below_tau_color] * data.shape[0]

    # logic to handle different color above tau vs below tau
    for i in range(data.shape[0] - 1):
        # slope between two points
        rise = data[i + 1, 1] - data[i, 1]
        run = data[i + 1, 0] - data[i, 0]
        # how much to scale slope to hit intersection
        scale = (tau - data[i, 1]) / rise
        delta_x = scale * run
        # both above tau
        if data[i, 1] > tau and data[i + 1, 1] > tau:
            ax.scatter(
                data[i : i + 2, 0],
                data[i : i + 2, 1],
                color=data_start_color,
                alpha=above_tau_alpha,
            )
            ax.plot(
                data[i : i + 2, 0],
                data[i : i + 2, 1],
                color=data_start_color,
                alpha=above_tau_alpha,
            )
        # above -> below tau
        elif data[i, 1] > tau and data[i + 1, 1] <= tau:
            ax.scatter(
                data[i, 0], data[i, 1], color=data_start_color, alpha=above_tau_alpha
            )
            ax.scatter(data[i + 1, 0], data[i + 1, 1], color=below_tau_color[i + 1])
            ax.plot(
                [data[i, 0], data[i, 0] + delta_x],
                [data[i, 1], tau],
                color=data_start_color,
                alpha=above_tau_alpha,
            )
            # use the lower tau index color
            ax.plot(
                [data[i, 0] + delta_x, data[i + 1, 0]],
                [tau, data[i + 1, 1]],
                color=below_tau_color[i + 1],
            )
        # below -> above tau
        elif data[i, 1] <= tau and data[i + 1, 1] > tau:
            ax.scatter(data[i, 0], data[i, 1], color=below_tau_color[i])
            ax.scatter(
                data[i + 1, 0],
                data[i + 1, 1],
                color=data_start_color,
                alpha=above_tau_alpha,
            )
            # use the lower tau index color
            ax.plot(
                [data[i, 0], data[i, 0] + delta_x],
                [data[i, 1], tau],
                color=below_tau_color[i],
            )
            ax.plot(
                [data[i, 0] + delta_x, data[i + 1, 0]],
                [tau, data[i + 1, 1]],
                color=data_start_color,
                alpha=above_tau_alpha,
            )
        # both below tau
        else:
            ax.scatter(data[i : i + 2, 0], data[i : i + 2, 1], color=below_tau_color[i])
            # use the lower tau index color
            if data[i, 1] <= data[i + 1, 1]:
                ax.plot(
                    data[i : i + 2, 0], data[i : i + 2, 1], color=below_tau_color[i]
                )
            else:
                ax.plot(
                    data[i : i + 2, 0], data[i : i + 2, 1], color=below_tau_color[i + 1]
                )

    ax.axhline(y=tau, color=tau_color, ls="--", lw=1.5, alpha=1, label="$\\tau$")
    ax.legend(loc="upper left", bbox_to_anchor=(1, 1.2))
    ax.set_title(title)

    return fig, ax


if __name__ == "__main__":
    # build gif

    data = np.array(
        [
            [0.0, 5.0],
            [1.0, 4.0],
            [2.5, 2.5],
            [4.5, 4.5],
            [6.5, 3.0],
            [9.5, 6.0],
            [11.5, 11.0],
            [13.5, 1.0],
        ]
    )

    BOUNDS = (0, 12)

    # save plot of signal
    ts = Signal(data[:, 1], times=data[:, 0])
    fig, ax = plt.subplots()
    ts.plot(ax)
    plt.savefig(os.path.join(OUT_PATH, "signal.png"), bbox_inches="tight", dpi=300)
    plt.close()

    # run signal persitence
    ts.make_pers(cutoff=-1)
    pers = ts.pers.diagram

    # hardcoded pause at the first persistence value
    middle_pause = pers.death.to_numpy().min()

    # the first part of the sweep
    for i, val in enumerate(
        np.linspace(data[:, 1].min() - 0.1, middle_pause, NUM_FRAMES // 2)
    ):
        # get components at the cutoff we're running for colors
        if SHOW_COMPONENTS:
            ts = Signal(data[:, 1], times=data[:, 0])
            ts.make_pers(cutoff=val + 1e-6)
            roots = ts.components.to_numpy().copy()
            all_roots(roots)
            colors = [plt.cm.Set1(j) for j in roots]
        else:
            colors = SINGLE_COLOR

        fig, (ax1, ax2) = plt.subplots(
            1, 2, figsize=(10, 3.5), gridspec_kw={"width_ratios": [6.0, 4.0]}
        )

        plot_signal_threshold(
            data,
            tau=val,
            fig=fig,
            ax=ax1,
            title="Signal\nThreshold ($\\tau$): {:.2f}".format(val),
            below_tau_color=colors,
        )
        # plot persistence values at or below val
        plot_persistence(
            pers.loc[pers.death <= val, :],
            bounds=BOUNDS,
            fig=fig,
            ax=ax2,
            color="black",
        )
        plt.savefig(
            os.path.join(OUT_PATH, "signal_sweep_pre_{:03}.png".format(i)),
            bbox_inches="tight",
        )

    # the rest of the sweep
    for i, val in enumerate(
        np.linspace(middle_pause, data[:, 1].max(), NUM_FRAMES // 2)
    ):
        # get components at the cutoff we're running for colors
        if SHOW_COMPONENTS:
            ts = Signal(data[:, 1], times=data[:, 0])
            ts.make_pers(cutoff=val + 1e-6)
            roots = ts.components.to_numpy().copy()
            all_roots(roots)
            colors = [plt.cm.Set1(j) for j in roots]
        else:
            colors = SINGLE_COLOR

        fig, (ax1, ax2) = plt.subplots(
            1, 2, figsize=(10, 3.5), gridspec_kw={"width_ratios": [6.0, 4.0]}
        )

        # breakpoint()
        plot_signal_threshold(
            data,
            tau=val,
            fig=fig,
            ax=ax1,
            title="Signal\nThreshold ($\\tau$): {:.2f}".format(val),
            below_tau_color=colors,
        )
        # plot persistence values at or below val
        plot_persistence(
            pers.loc[pers.death <= val, :],
            bounds=BOUNDS,
            fig=fig,
            ax=ax2,
            color="black",
        )
        plt.savefig(
            os.path.join(OUT_PATH, "signal_sweep_post_{:03}.png".format(i)),
            bbox_inches="tight",
        )

    # make the gif
    gif_name = "signal_sweep"
    if SHOW_COMPONENTS:
        gif_name += "_components"
    os.system(
        "convert -delay 30> -loop 0 {} -delay 300 {} -delay 30 {} -delay 300 {} {}.gif".format(
            os.path.join(OUT_PATH, "signal_sweep_pre_*"),
            sorted(glob(os.path.join(OUT_PATH, "signal_sweep_pre_*")))[-1],
            os.path.join(OUT_PATH, "signal_sweep_post_*"),
            sorted(glob(os.path.join(OUT_PATH, "signal_sweep_post_*")))[-1],
            os.path.join(OUT_PATH, gif_name),
        )
    )
