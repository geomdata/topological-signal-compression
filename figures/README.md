# figures

Version-controlled, pre-made figures for reference in notebooks.

* `signal_sweep_components.gif` - built from `./runners/signal_sweep.py`.

* `full_pers_signal.png` - built from `./runners/compression_toy_example.py`.

* `original_signal.gif` - built from `./runners/compression_toy_example.py`.

* `compressed_signal.gif` - built from `./runners/compression_toy_example.py`.