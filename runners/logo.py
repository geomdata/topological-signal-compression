"""
Script to make the logo for Topological Signal Compression.

Also makes gif of progressively-compressed logo.
"""

import os
from glob import glob

import matplotlib.pyplot as plt
import numpy as np

from tsc import compress_tsc

out_path = "./data/logo"
if not os.path.exists(out_path):
    os.mkdir(out_path)

compression_path = os.path.join(out_path, "compressions")
if not os.path.exists(compression_path):
    os.mkdir(compression_path)

# noise for y values
y_noise = 0.01

# values per letter
num_values = 200

rng = np.random.default_rng(0)

# T
side_t_values = num_values // 2
t_height = 0.5
t_start = 0
t_width = 2

T_y_values = np.array(
    list(np.repeat(t_height, side_t_values))
    + [0]
    + list(np.repeat(t_height, side_t_values))
)

# add y noise
T_y_values += rng.normal(scale=y_noise, size=T_y_values.size)

T_x_values = (
    list(np.linspace(t_start, t_start + t_width / 2, side_t_values))
    + [t_start + t_width / 2 + 0.01]
    + list(np.linspace(t_start + t_width / 2 + 0.01, t_width, side_t_values))
)

# S
s_start = 2.1
total_s_values = num_values

left_s_thetas = np.linspace(3 * np.pi / 2 - 0.5, 2 * np.pi, total_s_values // 2)
right_s_thetas = np.linspace(np.pi, np.pi / 2 - 0.5, total_s_values // 2)

S_x_values = np.r_[
    np.cos(left_s_thetas) / 2 + s_start, (np.cos(right_s_thetas) + 2) / 2 + s_start
]
S_y_values = np.r_[
    (np.sin(left_s_thetas) + 1) / 2 * t_height,
    (np.sin(right_s_thetas) + 1) / 2 * t_height,
]

# add y noise
S_y_values += rng.normal(scale=y_noise, size=S_y_values.size)

# C
c_start = 4.75
total_c_values = num_values
bottom_theta_values = np.linspace(3 * np.pi / 2 + 0.5, np.pi, total_c_values // 2)
top_theta_values = np.linspace(np.pi, np.pi / 2 - 0.5, total_c_values // 2)

C_x_values = np.r_[
    np.cos(bottom_theta_values) + c_start, np.cos(top_theta_values) + c_start
]
C_y_values = (
    np.r_[np.sin(bottom_theta_values), np.sin(top_theta_values)] / 2 + 0.5
) * t_height

# add y noise
C_y_values += rng.normal(scale=y_noise, size=C_y_values.size)

fig, ax = plt.subplots(figsize=(8, 3))
ax.plot(T_x_values, T_y_values, c="black")
ax.plot(S_x_values, S_y_values, c="black")
ax.plot(C_x_values, C_y_values, c="black")
ax.axis("off")
fig.set_facecolor("None")
ax.text(
    0.5,
    -0.15,
    "Topological Signal Compression",
    ha="center",
    size=20,
    family="monospace",
    style="oblique",
    transform=ax.transAxes,
)
plt.savefig(os.path.join(out_path, "logo.png"), dpi=200, bbox_inches="tight")
plt.close(fig)

# compress several levels and save
num_compressions = 10
compressions = np.linspace(0, 0.02, 10)
for persistence_cutoff in compressions:
    compressed_t = compress_tsc(
        np.c_[T_x_values, T_y_values], persistence_cutoff=persistence_cutoff
    )
    compressed_s = compress_tsc(
        np.c_[S_x_values, S_y_values], persistence_cutoff=persistence_cutoff
    )
    compressed_c = compress_tsc(
        np.c_[C_x_values, C_y_values], persistence_cutoff=persistence_cutoff
    )

    fig, ax = plt.subplots(figsize=(8, 3))
    ax.plot(compressed_t[:, 0], compressed_t[:, 1], c="black")
    ax.plot(compressed_s[:, 0], compressed_s[:, 1], c="black")
    ax.plot(compressed_c[:, 0], compressed_c[:, 1], c="black")
    ax.axis("off")
    plt.savefig(
        os.path.join(compression_path, f"logo_cutoff_{persistence_cutoff}.png"),
        bbox_inches="tight",
    )
    plt.close(fig)

# make gif
os.system(
    "convert -loop 0 -delay 30 {} -delay 100 {} {}.gif".format(
        os.path.join(compression_path, "*.png"),
        sorted(glob(os.path.join(compression_path, "*.png")))[-1],
        os.path.join(out_path, "logo"),
    )
)
