"""
Tests for ``tsc.viz.py``.
"""

import numpy as np
import pandas as pd
import pytest

from tsc import signal_persistence
from tsc.utils.viz import plot_persistence


def test_plot_persistence() -> None:
    """
    Make sure ``tsc.viz.plot_persistence()`` behaves as expected.
    """
    data = np.array(
        [
            [0.0, 5.0],
            [1.0, 4.0],
            [2.5, 2.5],
            [4.5, 4.5],
            [6.5, 3.0],
            [9.5, 6.0],
            [11.5, 11.0],
            [13.5, 1.0],
        ]
    )

    pers = signal_persistence(data)

    try:
        fig, ax = plot_persistence(pers_data=pers, bounds=(0, 12), figsize=(10, 10))
        plot_persistence(
            pers_data=pers,
            bounds=None,
            fig=fig,
            ax=ax,
            birth_death_line_kwargs={"c": "green", "ls": "--"},
            c="blue",
            s=20,
        )
        assert True
    except:  # noqa: E722
        pytest.fail("No failures of any kind should have happened from those calls...")

    # should be able to handle empty df with pre-specified bounds
    df = pd.DataFrame()
    plot_persistence(pers_data=df, bounds=(0, 5))
    assert True

    # cannot hand function only one of fig, ax
    try:
        plot_persistence(pers_data=pers, fig=fig, ax=None)
        pytest.fail("should have raised `ValueError`.")
    except ValueError:
        assert True
