"""
Tests for ``tsc.utils.data.py``.
"""

import numpy as np

from tsc.utils.data import chopin, fsdd


def test_fsdd() -> None:
    """
    Make sure ``tsc.utils.data.fsdd()`` behaves as expected.
    """
    num_examples = 10

    signals, labels = fsdd(dataset_size=num_examples)

    assert isinstance(signals[0], np.ndarray)
    assert isinstance(labels[0], int)

    assert len(signals) == len(labels) == num_examples

    signals, labels = fsdd(dataset_size=999999)

    # total number of valid examples we use
    assert len(signals) == len(labels) == 2816


def test_chopin() -> None:
    """
    Make sure ``tsc.utils.data.chopin()`` behaves as expected.
    """
    from pydub import AudioSegment

    snippet = chopin()

    assert isinstance(snippet, AudioSegment)
    assert snippet.frame_rate == 44100
    assert snippet.duration_seconds == 10
