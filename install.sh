#!/bin/bash
# kill any old, existing env and notebook kernel
bash uninstall.sh || echo "Failed to Uninstall"
mamba env create -f tsc.yml || conda env create -f tsc.yml
# only do follow-up installation if environment can be activated (e.g. the previous line actually succeeded)
source activate tsc && \
  pip install -e .[extras,testing,docs,ruff] && \
  python -m ipykernel install --user --name tsc --display-name "Python (tsc)"
