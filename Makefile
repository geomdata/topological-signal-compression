.PHONY: badges format docs test test-nb view-docs view-tests

.ONESHELL:
SHELL := /bin/bash

TEST_ARGS?=""

badges:
	if [ "${CONDA_DEFAULT_ENV}" != "tsc" ]; then source activate tsc; fi
	python .gitlab/make_badges.py

format:
	if [ "${CONDA_DEFAULT_ENV}" != "tsc" ]; then source activate tsc; fi
	ruff format
	ruff check --fix

cleandocs:
	rm -f docs/source/*.ipynb

docs: cleandocs
	if [ "${CONDA_DEFAULT_ENV}" != "tsc" ]; then source activate tsc; fi
	bash build_sphinx_docs.sh

docs-strict: cleandocs
	if [ "${CONDA_DEFAULT_ENV}" != "tsc" ]; then source activate tsc; fi
	bash build_sphinx_docs -W

install:
	bash install.sh

test:
	if [ "${CONDA_DEFAULT_ENV}" != "tsc" ]; then source activate tsc; fi
	pytest -c pytest.ini $(TEST_ARGS)

test-all: test view-tests test-nb view-tests-nb

test-nb:
	if [ "${CONDA_DEFAULT_ENV}" != "tsc" ]; then source activate tsc; fi
	pytest -c tests/pytest_examples.ini

uninstall:
	bash uninstall.sh

view-docs:
	xdg-open public/index.html

view-tests:
	xdg-open data/pytest/report_tsc.html

view-tests-nb:
	xdg-open data/pytest/report_examples.html
